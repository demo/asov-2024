{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8179151a",
   "metadata": {},
   "source": [
    "# RDF exploration for ObsParis published resources\n",
    "\n",
    "Since 2019, ObsParis is publishing resources using DOIs. The goal of this experiment is to explore several aspects:\n",
    "- validate the resource metadata  \n",
    "- track the resource citations and references \n",
    "- connect with the IVOA registry and interfaces distributing the resources \n",
    "\n",
    "## Methodology\n",
    "\n",
    "We have selected RDF for representing and storing the data in the form of a graph, and we use `rdflib` to interact with this graph.\n",
    "\n",
    "RDF is modeling the information using _triples_ forming a statement (Subject, Predicate, Object). \n",
    "\n",
    "The `Report` class is deriving from the RDFLib `Graph` class, and adds convenient methods for this study.\n",
    "\n",
    "## Results\n",
    "\n",
    "The first outcome is the validation of the DOI metadata, since the automated processing implies some validation (e.g., for each DOI, seeking for the registration agency leads to spot invalid DOIs). What we fixed: \n",
    "- typos in related identifiers and person identifiers (e.g., leading space)\n",
    "- in related identifiers: DOIs seen as URLs\n",
    "\n",
    "Tracking the dataset impact can be derived from a SPARQL Query. For instance, getting the citations of all datasets with a given author can be written as:\n",
    "```\n",
    "SELECT DISTINCT ?data_doi ?relation ?bib_reference\n",
    "WHERE {\n",
    "    ?data_doi dcterms:creator <https://orcid.org/0000-0002-9552-8822> .\n",
    "    { ?bib_reference ?relation ?data_doi .\n",
    "    FILTER (?relation IN (ivoarel:Cites, ivoarel:IsSupplementedBy, dcterms:references ) )\n",
    "    }\n",
    "}\n",
    "```\n",
    "\n",
    "Connection with the IVOA registry metadata allows to go one step further:\n",
    "\n",
    "```\n",
    "SELECT DISTINCT ?data_doi ?tapurl ?tablename\n",
    "WHERE {\n",
    "    ?doi_doi dcat:inCatalog ?ivo .\n",
    "    ?ivo a dcat:Catalog ; \n",
    "        dcat:accessService ?t ;\n",
    "        rdfs:label ?tablename .\n",
    "    ?t a dcat:DataService ;\n",
    "        dcat:endpointURL ?tapurl ;\n",
    "        dcterms:conformsTo <ivo://ivoa.net/std/TAP> .\n",
    "    {?ref ?p ?data_doi .\n",
    "    FILTER (?p IN (ivoarel:Cites, ivoarel:IsSupplementedBy, dcterms:references ) )\n",
    "    }\n",
    "}\n",
    "```\n",
    "\n",
    "\n",
    "\n",
    "## Sources of information\n",
    "\n",
    "### DataCite\n",
    "\n",
    "ObsParis is registered with DataCite for obtaining DOIs for their resources. DataCite is thus the primary source of information to retreive the metadata FOR ObsParis DOI.\n",
    "\n",
    "The method `Report.get_obsparis_dois()` is querying the DataCite API: \n",
    "- it lists the DOIs.\n",
    "- it sets the resource type using `schema.org` and `DCMI` types. \n",
    "- it retreives the resource title and creators (including their ORICD if listed)\n",
    "\n",
    "### Crossref Event Data\n",
    "\n",
    "Crossref EventData is storing \"events\" related to digital objects registered by Crossref (mostly journal articles). \n",
    "\n",
    "### Scholexplorer\n",
    "\n",
    "The Scholexplorer API is collecting information from various sources and present them using a `dcterms:isReferencedBy` relation. \n",
    "\n",
    "### OpenCitation\n",
    "\n",
    "OpenCitation is a tool to explore citations between digital resources identified by their PIds.\n",
    "\n",
    "### BibLinks\n",
    "\n",
    "BibLinks is a prototype interface to share relations by data provides. The two relations types currently accepted are: `ivoarel:Cites` and `ivoarel:IsSupplementedBy`. \n",
    "\n",
    "## Requirements\n",
    "This notebook requires `rdflib` and `resquests` packages.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "270ccdf9",
   "metadata": {},
   "source": [
    "## 1. Getting initial data from DataCite"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91fef202",
   "metadata": {},
   "outputs": [],
   "source": [
    "from obsparis_skg_demo import Report"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f9ea09c",
   "metadata": {},
   "outputs": [],
   "source": [
    "report = Report()\n",
    "report.get_obsparis_dois()\n",
    "#report.get_dois_from_prefix(\"10.25935\")\n",
    "#report.get_dois_from_prefix(\"10.48322\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2a5124f",
   "metadata": {},
   "source": [
    "### A few examples of output"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4be96f68",
   "metadata": {},
   "source": [
    "#### List of namespaces in the graph\n",
    "\n",
    "> Method on the Graph class (The Report class is a Graph with custom methods)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8e0e7417",
   "metadata": {},
   "outputs": [],
   "source": [
    "for ns in report.namespaces():\n",
    "    print(ns)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1892e20",
   "metadata": {},
   "source": [
    "#### Extract data for a DOI\n",
    "\n",
    "> Iterator on the graph\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7cc73af3",
   "metadata": {},
   "outputs": [],
   "source": [
    "from rdflib import URIRef, Graph\n",
    "from obsparis_skg_demo import BIBLINK, DCITE\n",
    "\n",
    "s = URIRef(\"https://doi.org/10.25935/2f8g-c505\")\n",
    "\n",
    "g = Graph()\n",
    "g.bind(\"biblink\", BIBLINK)\n",
    "g.bind(\"dcite\", DCITE)\n",
    "for p, o in report.predicate_objects(subject=URIRef(\"https://doi.org/10.25935/2f8g-c505\")):\n",
    "    g.add((s, p, o))\n",
    "\n",
    "print(g.serialize())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1b51b9a",
   "metadata": {},
   "source": [
    "#### List all resources linked to an author (identified with its ORCID)\n",
    "\n",
    "> SPARQL query on the graph \n",
    "\n",
    "**Query:** \n",
    "- find all `?doi` with a `dcterms:creator` property with value `https://orcid.org/0000-0002-9552-8822`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bcc4f4d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "orcid_query = \"\"\"\n",
    "SELECT DISTINCT ?doi \n",
    "WHERE {\n",
    "    ?doi dcterms:creator <https://orcid.org/0000-0002-9552-8822> ;\n",
    "}\"\"\"\n",
    "\n",
    "\n",
    "result = report.query(orcid_query)\n",
    "for row in result:\n",
    "    print(str(row[0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "076bd75d",
   "metadata": {},
   "source": [
    "List all DOIs of the Graph (filter: all DOIs have biblink:scheme = \"doi\" property)\n",
    "\n",
    "> SPARQL query with specific namespace\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e544929",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "doi_query = \"\"\"\n",
    "SELECT DISTINCT ?doi \n",
    "WHERE {\n",
    "    ?doi biblink:scheme \"doi\" . \n",
    "}\"\"\"\n",
    "\n",
    "result = report.query(doi_query, initNs={\"biblink\": \"http://www.ivoa.net/rdf/biblink#\"})\n",
    "for row in result:\n",
    "    print(str(row[0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "532e90ed",
   "metadata": {},
   "source": [
    "#### Serialize in Turtle"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbd3a2f1",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(report.serialize())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e0b07c7f",
   "metadata": {},
   "source": [
    "#### List links from a node:\n",
    "\n",
    "**NB**: blank nodes are referred to as ther internal node ID\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b8528d98",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "for p, o in report[URIRef('https://doi.org/10.25935/temb-ys31')]:\n",
    "    print(p, o)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebc07205",
   "metadata": {},
   "source": [
    "## 2. Importing from external SKG\n",
    "\n",
    "### IVOA BibLinks (MASER prototype)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d40f3873",
   "metadata": {},
   "outputs": [],
   "source": [
    "biblink_url = \"http://voparis-tap-maser.obspm.fr/__system__/biblinks/links/biblinks.json\"\n",
    "report.get_biblinks(access_url=biblink_url)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7741069",
   "metadata": {},
   "source": [
    "#### List all resources linked to an author (identified with its ORCID)\n",
    "\n",
    "> SPARQL query on the graph \n",
    "\n",
    "**Query** \n",
    "- all `?doi` with a `dcterms:creator` property with value `https://orcid.org/0000-0002-9552-8822`\n",
    "- all `?doi` that are object of a triple linking a reference to them with 2 properties (`ivoarel:Cites` and `ivoarel:IsSupplementedBy`)\n",
    "- keep the property linking the reference to the data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "844c52eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "orcid_query = \"\"\"\n",
    "SELECT DISTINCT ?doi ?p ?ref \n",
    "WHERE {\n",
    "    ?doi dcterms:creator <https://orcid.org/0000-0002-9552-8822> .\n",
    "    ?ref ?p ?doi .\n",
    "    FILTER (?p IN (ivoarel:Cites, ivoarel:IsSupplementedBy ) )\n",
    "}\"\"\"\n",
    "\n",
    "\n",
    "result = report.query(orcid_query)\n",
    "for row in result:\n",
    "    print(f\"{str(row[0])} {str(row[1].split('#')[1])} {str(row[2])}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89e233d8",
   "metadata": {},
   "source": [
    "#### Serialize in Turtle"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d555c8d2",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(report.serialize())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ccd32521",
   "metadata": {},
   "source": [
    "### Scholexplorer API"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "645eec46",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "report.get_scholexplorer()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f3d3eec",
   "metadata": {},
   "source": [
    "#### List all resources linked to an author (identified with its ORCID)\n",
    "\n",
    "> SPARQL query on the graph \n",
    "\n",
    "**Query** \n",
    "- all `?doi` with a `dcterms:creator` property with value `https://orcid.org/0000-0002-9552-8822`\n",
    "- all `?doi` that are object of a triple linking a reference to them with 2 properties (`ivoarel:Cites` and `ivoarel:IsSupplementedBy`)\n",
    "- keep the property linking the reference to the data\n",
    " \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d2aad0ec",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "orcid_query = \"\"\"\n",
    "SELECT DISTINCT ?doi ?p ?ref \n",
    "WHERE {\n",
    "    ?doi dcterms:creator <https://orcid.org/0000-0002-9552-8822> .\n",
    "    { ?ref ?p ?doi .\n",
    "    FILTER (?p IN (ivoarel:Cites, ivoarel:IsSupplementedBy, dcterms:references ) )\n",
    "    }\n",
    "}\n",
    "\"\"\"\n",
    "result = report.query(orcid_query)\n",
    "for row in result:\n",
    "    subject_id, predicate, object_id = row\n",
    "    if \"#\" in str(predicate):\n",
    "        predicate = str(predicate).split('#')[1]\n",
    "    else:\n",
    "        predicate = str(predicate).split('/')[-1]\n",
    "    print(f\"{str(subject_id)} {predicate} {str(object_id)}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ebf48266",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(report.serialize())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28f8e5c9",
   "metadata": {},
   "source": [
    "### OpenCitation API"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f427673b",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "report.get_opencitation()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c806ef15",
   "metadata": {},
   "source": [
    "#### List all resources linked to an author (identified with its ORCID)\n",
    "\n",
    "> SPARQL query on the graph \n",
    "\n",
    "**Query** \n",
    "- all `?doi` with a `dcterms:creator` property with value `https://orcid.org/0000-0002-9552-8822`\n",
    "- all `?doi` that are object of a triple linking a reference to them with 2 properties (`ivoarel:Cites` and `ivoarel:IsSupplementedBy`)\n",
    "- keep the property linking the reference to the data\n",
    "- get the source of the information\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08349c84",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "orcid_query = \"\"\"\n",
    "SELECT ?ref ?p ?doi ?prov\n",
    "WHERE {\n",
    "    ?doi dcterms:creator <https://orcid.org/0000-0002-9552-8822> .\n",
    "    ?ref ?p ?doi .\n",
    "    ?s a rdf:Statement ;\n",
    "        rdf:subject ?ref ;\n",
    "        rdf:predicate ?p ;\n",
    "        rdf:object ?doi ;\n",
    "        prov:wasInformedBy ?prov.\n",
    "    FILTER (?p IN (ivoarel:Cites, ivoarel:IsSupplementedBy, dcterms:references ) )\n",
    "}\"\"\"\n",
    "\n",
    "result = report.query(orcid_query)\n",
    "for row in result:\n",
    "    subject_id, predicate, object_id, prov_val = row\n",
    "    if \"#\" in str(predicate):\n",
    "        predicate = str(predicate).split('#')[1]\n",
    "    else:\n",
    "        predicate = str(predicate).split('/')[-1]\n",
    "    print(f\"{str(subject_id)} {predicate} {str(object_id)} ({str(prov_val)})\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1d42464",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(report.serialize())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e42801bf",
   "metadata": {},
   "source": [
    "### Crossref EventData API"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1ecd905",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "report.get_crossref_eventdata()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69f45616",
   "metadata": {},
   "source": [
    "#### List all resources linked to an author (identified with its ORCID)\n",
    "\n",
    "> SPARQL query on the graph \n",
    "\n",
    "**Query** \n",
    "- all `?doi` with a `dcterms:creator` property with value `https://orcid.org/0000-0002-9552-8822`\n",
    "- all `?doi` that are object of a triple linking a reference to them with 2 properties (`ivoarel:Cites` and `ivoarel:IsSupplementedBy`)\n",
    "- keep the property linking the reference to the data\n",
    "- get the source of the information\n",
    "\n",
    "Then export the graph into a turtle file.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4404fa5",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from obsparis_skg_demo import IVOAREL\n",
    "from rdflib import DCTERMS\n",
    "\n",
    "orcid_query = \"\"\"\n",
    "SELECT ?ref ?p ?doi ?prov\n",
    "WHERE {\n",
    "    ?doi dcterms:creator <https://orcid.org/0000-0002-9552-8822> .\n",
    "    ?ref ?p ?doi .\n",
    "    ?s a rdf:Statement ;\n",
    "        rdf:subject ?ref ;\n",
    "        rdf:predicate ?p ;\n",
    "        rdf:object ?doi ;\n",
    "        prov:wasInformedBy ?prov.\n",
    "    FILTER (?p IN (ivoarel:Cites, ivoarel:IsSupplementedBy, dcterms:references ) )\n",
    "}\"\"\"\n",
    "\n",
    "result = report.query(orcid_query)\n",
    "\n",
    "citations = Graph()\n",
    "citations.bind(\"ivoarel\", IVOAREL)\n",
    "\n",
    "for row in result:\n",
    "    subject_id, predicate, object_id, prov_val = row\n",
    "    if \"#\" in str(predicate):\n",
    "        predicate = str(predicate).split('#')[1]\n",
    "    else:\n",
    "        predicate = str(predicate).split('/')[-1]\n",
    "    print(f\"{str(subject_id)} {predicate} {str(object_id)} ({str(prov_val)})\")\n",
    "\n",
    "    if predicate == DCTERMS.isReferencedBy:\n",
    "        citations.add((object_id, DCTERMS.references, subject_id))\n",
    "    else:\n",
    "        citations.add((object_id, DCTERMS.isReferencedBy, subject_id))\n",
    "\n",
    "citations.serialize(\"citations.ttl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "762fa19e",
   "metadata": {},
   "source": [
    "#### Finally export the full graph in RDF/Turtle"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "690a3929",
   "metadata": {},
   "outputs": [],
   "source": [
    "report.serialize('report.ttl')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ea41edd",
   "metadata": {},
   "source": [
    "### Extra functions\n",
    "\n",
    "- `get_registrar()`: retreive the DOI registrar\n",
    "- `get_pids_by_scheme()`: get the PID list from the graph (filtering by PID scheme)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a6b34d3",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_registrar(doi):\n",
    "    import requests \n",
    "    \n",
    "    API_URL = \"https://dx.doi.org/ra/\"\n",
    "    try: \n",
    "        access_url = f\"{API_URL}{doi}\"\n",
    "        response = requests.get(access_url)\n",
    "        data = response.json()\n",
    "        return data[0]['RA']\n",
    "    except JSONDecodeError as e:\n",
    "        print(doi, e)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4fcfdccf",
   "metadata": {},
   "outputs": [],
   "source": [
    "PID_SCHEMES = [\"doi\", \"arXiv\", \"bibcode\", \"pmc\", \"pmid\", \"handle\", \"orcid\"]\n",
    "from rdflib.plugins.sparql import prepareQuery\n",
    "from rdflib import Literal\n",
    "\n",
    "def get_pids_by_scheme(report, pid_scheme):\n",
    "    if pid_scheme not in PID_SCHEMES:\n",
    "        raise ValueError(f\"Invalid pid_scheme. Valid schemes: {', '.join(PID_SCHEMES)}\")\n",
    "    \n",
    "    q = prepareQuery(\n",
    "        \"SELECT DISTINCT ?pid WHERE { ?pid biblink:scheme ?scheme .}\",\n",
    "        initNs = { \"biblink\": BIBLINK }\n",
    "    )\n",
    "\n",
    "    results = report.query(q, initBindings={\"scheme\": Literal(pid_scheme)})\n",
    "    return results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ce8bca0",
   "metadata": {},
   "source": [
    "List of `bibcode` PIDs in the graph:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ce2f0a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "for item in get_pids_by_scheme(report, \"bibcode\"):\n",
    "    print(item)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68b2e422",
   "metadata": {},
   "source": [
    "### First try at desambiguating\n",
    "\n",
    "- `ArXiv` identifier and `ArXiv` DOI\n",
    "- PubMed identifiers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "524cad63",
   "metadata": {},
   "outputs": [],
   "source": [
    "def desambiguate(report):\n",
    "\n",
    "    from rdflib import OWL, PROV \n",
    "    import requests\n",
    "\n",
    "    # arXiv URLs and arXiv DOIs:\n",
    "    arXiv_ids = get_pids_by_scheme(report, \"arXiv\")\n",
    "    dois = get_pids_by_scheme(report, \"doi\")\n",
    "    for item in dois:\n",
    "        doi = str(item[0])\n",
    "        for arXiv_id in arXiv_ids:\n",
    "            if doi.endswith(arXiv_id[0].split(\"/\")[-1]):\n",
    "                print(arXiv_id[0], '=', item[0])\n",
    "                report.add((arXiv_id[0], OWL.sameAs, item[0]))\n",
    "                report.add((item[0], OWL.sameAs, arXiv_id[0]))\n",
    "        \n",
    "    # PMID/PMC IDs and DOIs using PMC API:\n",
    "    API_URL = \"https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=script&email=baptiste.cecconi@obspm.fr&format=json&\"\n",
    "    \n",
    "    pmid_urls = get_pids_by_scheme(report, \"pmid\")\n",
    "    for item in pmid_urls:\n",
    "        pmid = item[0].split('/')[-2]\n",
    "        access_url = API_URL+f\"ids={pmid}\"\n",
    "        print(access_url)\n",
    "        response = requests.get(access_url)\n",
    "        data = response.json()\n",
    "        doi = URIRef(data['records'][0][\"doi\"].lower())\n",
    "        print(item[0], '=', doi)\n",
    "        report.add_with_prov(\n",
    "            (item[0], OWL.sameAs, doi),\n",
    "            {PROV.wasInformedBy: Literal(\"PubMedCentral\")}\n",
    "        )\n",
    "        report.add_with_prov(\n",
    "            (doi, OWL.sameAs, item[0]),\n",
    "            {PROV.wasInformedBy: Literal(\"PubMedCentral\")}\n",
    "        )\n",
    "\n",
    "    pmc_urls = get_pids_by_scheme(report, \"pmc\")\n",
    "    for item in pmc_urls:\n",
    "        pmc = item[0].split('/')[-2]\n",
    "        access_url = API_URL+f\"ids={pmc}\"\n",
    "        print(access_url)\n",
    "        response = requests.get(access_url)\n",
    "        data = response.json()\n",
    "        doi = URIRef(data['records'][0][\"doi\"].lower())\n",
    "        print(item[0], '=', doi)\n",
    "        report.add_with_prov(\n",
    "            (item[0], OWL.sameAs, doi),\n",
    "            {PROV.wasInformedBy: Literal(\"PubMedCentral\")}\n",
    "        )\n",
    "        report.add_with_prov(\n",
    "            (doi, OWL.sameAs, item[0]),\n",
    "            {PROV.wasInformedBy: Literal(\"PubMedCentral\")}\n",
    "        )\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd83a8a8",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "desambiguate(report)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "55a101b1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_pids(report, pid_scheme=None):\n",
    "    \n",
    "    if pid_scheme is None:\n",
    "        pid_schemes = PID_SCHEMES\n",
    "    else:\n",
    "        if isinstance(pid_scheme, str):\n",
    "            pid_schemes = [pid_scheme]\n",
    "        if not isinstance(pid_scheme, list):\n",
    "            raise TypeError(\"pid_scheme: List[str] | str\")\n",
    "        else:\n",
    "            if any([item not in valid_pid_schemes for item in pid_scheme]):\n",
    "                raise \n",
    "\n",
    "    pids = []\n",
    "    for item in pid_schemes:\n",
    "        pids.extend(get_pids_by_scheme(report, item))\n",
    "    return pids\n",
    "\n",
    "def add_doi_registrar(report):\n",
    "    \n",
    "    from rdflib import PROV \n",
    "    \n",
    "    pids = get_pids_by_scheme(report, \"doi\")\n",
    "\n",
    "    for pid in pids:\n",
    "        doi = str(pid[0]).replace(\"https://doi.org/\", \"\")\n",
    "        try:\n",
    "            agency = get_registrar(doi)\n",
    "        except KeyError:\n",
    "            print(doi)\n",
    "        print(doi, agency)\n",
    "        report.add((row[0], PROV.wasInformedBy, Literal(agency)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0334e708",
   "metadata": {},
   "source": [
    "### Include the DOI registrar\n",
    "\n",
    "Helps to make sure there is no typo in DOIs...\n",
    "(there were a few...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c98f47a",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "add_doi_registrar(report)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28f91354",
   "metadata": {},
   "outputs": [],
   "source": [
    "report.serialize('report.ttl')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0f4fc90",
   "metadata": {},
   "source": [
    "## 3. IVOA Registry metadata"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "017c05bf",
   "metadata": {},
   "source": [
    "Example for dataset referenced in `https://doi.org/10.3847/1538-4357/ac0af1`\n",
    "\n",
    "### First explore list of linked datasets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b16e0140",
   "metadata": {},
   "outputs": [],
   "source": [
    "from rdflib import URIRef, Graph\n",
    "from obsparis_skg_demo import BIBLINK, DCITE\n",
    "\n",
    "s = URIRef(\"https://doi.org/10.3847/1538-4357/ac0af1\")\n",
    "\n",
    "g = Graph()\n",
    "g.bind(\"biblink\", BIBLINK)\n",
    "g.bind(\"dcite\", DCITE)\n",
    "for p, o in report.predicate_objects(subject=s):\n",
    "    g.add((s, p, o))\n",
    "    \n",
    "print(\"RDF/Turtle:\\n\")\n",
    "print(g.serialize())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "957bd571",
   "metadata": {},
   "source": [
    "There are 3 dataset: `https://doi.org/10.25935/5jfx-dh49`, `https://doi.org/10.25935/xs9j-nd90` and `https://doi.org/10.25935/zmmz-ca38`. \n",
    "\n",
    "They are available through various TAP interfaces:\n",
    "- `cassini_rpws.epn_core` table for  `https://doi.org/10.25935/5jfx-dh49` and `https://doi.org/10.25935/xs9j-nd90`\n",
    "- `tfcat.epn_core` table for  `https://doi.org/10.25935/zmmz-ca38`\n",
    "- `tfcat.wu_nkom_saturn` table for `https://doi.org/10.25935/zmmz-ca38`\n",
    "\n",
    "Using `DCAT`, we can describe:\n",
    "- dataset as as `DCAT.Dataset`\n",
    "- EPNcore tables as `DCAT.Catalog`\n",
    "- Other tables (containing actual data) as `DCAT.Distribution`\n",
    "\n",
    "### Add triples for these objects and properties "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a395d73f",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "from rdflib import RDF, RDFS, SDO, DCAT\n",
    "\n",
    "# define the nodes to be updated\n",
    "\n",
    "dataset_1 = URIRef(\"https://doi.org/10.25935/xs9j-nd90\")\n",
    "dataset_2 = URIRef(\"https://doi.org/10.25935/5jfx-dh49\")\n",
    "dataset_3 = URIRef(\"https://doi.org/10.25935/zmmz-ca38\")\n",
    "\n",
    "cassini_rpws_epn_core = URIRef(\"ivo://padc.obspm.maser/cassini_rpws/epn/epn_core\")\n",
    "tfcat_wu_nkom_saturn = URIRef(\"ivo://padc.obspm.maser/tfcat/q/wu_nkom_saturn\")\n",
    "tfcat_epn_core = URIRef(\"ivo://padc.obspm.maser/tfcat/q/epn_core\")\n",
    "\n",
    "tap_server_uri = URIRef(\"ivo://padc.obspm.maser/tap\")\n",
    "epncore_std_uri = URIRef(\"ivo://ivoa.net/std/epntap#table-2.0\")\n",
    "tap_std_uri = URIRef(\"ivo://ivoa.net/std/TAP\")\n",
    "\n",
    "# Add the links between the nodes and the relevant properties \n",
    "\n",
    "report.add((dataset_1, RDF.type, DCAT.Dataset))\n",
    "report.add((dataset_1, RDF.type, SDO.Dataset))\n",
    "report.add((dataset_1, DCAT.inCatalog, cassini_rpws_epn_core))\n",
    "\n",
    "report.add((dataset_2, RDF.type, DCAT.Dataset))\n",
    "report.add((dataset_2, RDF.type, SDO.Dataset))\n",
    "report.add((dataset_2, DCAT.inCatalog, cassini_rpws_epn_core))\n",
    "\n",
    "report.add((dataset_3, RDF.type, DCAT.Dataset))\n",
    "report.add((dataset_3, RDF.type, SDO.Dataset))\n",
    "report.add((dataset_3, DCAT.distribution, tfcat_wu_nkom_saturn))\n",
    "report.add((dataset_3, DCAT.inCatalog, tfcat_epn_core))\n",
    "\n",
    "report.add((cassini_rpws_epn_core, RDF.type, DCAT.Catalog))\n",
    "report.add((cassini_rpws_epn_core, DCTERMS.title, Literal(\"Cassini/RPWS Kronos Database\", lang=\"en\")))\n",
    "report.add((cassini_rpws_epn_core, DCAT.accessService, tap_server_uri))\n",
    "report.add((cassini_rpws_epn_core, DCAT.mediaType, Literal(\"application/x-votable+xml\")))\n",
    "report.add((cassini_rpws_epn_core, DCTERMS.conformsTo, epncore_std_uri))\n",
    "report.add((cassini_rpws_epn_core, RDFS.label, Literal(\"cassini_rpws.epn_core\")))\n",
    "\n",
    "report.add((tfcat_wu_nkom_saturn, RDF.type, DCAT.Distribution))\n",
    "report.add((tfcat_wu_nkom_saturn, DCTERMS.title, Literal(\"Wu et al. (2022) nkom Saturn catalogue.\", lang=\"en\")))\n",
    "report.add((tfcat_wu_nkom_saturn, DCAT.accessService, tap_server_uri))\n",
    "report.add((tfcat_wu_nkom_saturn, DCAT.mediaType, Literal(\"application/x-votable+xml\")))\n",
    "report.add((tfcat_wu_nkom_saturn, RDFS.label, Literal(\"tfcat.wu_nkom_saturn\")))\n",
    "\n",
    "report.add((tfcat_epn_core, RDF.type, DCAT.Catalog))\n",
    "report.add((tfcat_epn_core, DCTERMS.title, Literal(\"MASER TFCat Database\", lang=\"en\")))\n",
    "report.add((tfcat_epn_core, DCAT.accessService, tap_server_uri))\n",
    "report.add((tfcat_epn_core, DCAT.mediaType, Literal(\"application/x-votable+xml\")))\n",
    "report.add((tfcat_epn_core, DCTERMS.conformsTo, epncore_std_uri))\n",
    "report.add((tfcat_epn_core, RDFS.label, Literal(\"tfcat.epn_core\")))\n",
    "\n",
    "report.add((tap_server_uri, RDF.type, DCAT.DataService))\n",
    "report.add((tap_server_uri, DCAT.endpointDescription, Literal(\"PADC TAP Server on voparis-tap-maser.obspm.fr TAP service\", lang=\"en\")))  \n",
    "report.add((tap_server_uri, DCAT.endpointURL, URIRef(\"http://voparis-tap-maser.obspm.fr/tap\")))\n",
    "report.add((tap_server_uri, DCTERMS.conformsTo, tap_std_uri))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4c5e731",
   "metadata": {},
   "outputs": [],
   "source": [
    "report.serialize(\"report.ttl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d25195d",
   "metadata": {},
   "source": [
    "### Extract information using SPARQL Query\n",
    "\n",
    "**Query**: get the dataset DOIs referenced in `https://doi.org/10.3847/1538-4357/ac0af1` and that are acessible through a TAP interface: \n",
    "- all `?doi` referenced by `https://doi.org/10.3847/1538-4357/ac0af1`\n",
    "```\n",
    "    ?ref ?p ?doi .\n",
    "```\n",
    "\n",
    "- filter `?doi` has a link (`dcat:inCatalog` or `dcat:distribution`) to a resource, with a label `?tablename` and  a `dcat:accessService`\n",
    "\n",
    "```\n",
    "    ?doi ?d ?ivo .\n",
    "    ?ivo rdf:type ?dcat ; \n",
    "        dcat:accessService ?tap ;\n",
    "        rdfs:label ?tablename .\n",
    "    FILTER (?d IN (dcat:inCatalog, dcat:distribution ) )\n",
    "```\n",
    "\n",
    "- compliant with `TAP`. We also want to get the `dcat:endpointURL` of the TAP server.\n",
    "```\n",
    "    ?tap a dcat:DataService ;\n",
    "        dcat:endpointURL ?tapurl ;\n",
    "        dcterms:conformsTo <ivo://ivoa.net/std/TAP> .\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33fa16f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = report.query(\"\"\"\n",
    "SELECT DISTINCT ?doi ?dcat ?tapurl ?tablename\n",
    "WHERE {\n",
    "    ?ref ?p ?doi .\n",
    "    ?doi ?d ?ivo .\n",
    "    ?ivo rdf:type ?dcat ; \n",
    "        dcat:accessService ?tap ;\n",
    "        rdfs:label ?tablename .\n",
    "    ?tap a dcat:DataService ;\n",
    "        dcat:endpointURL ?tapurl ;\n",
    "        dcterms:conformsTo <ivo://ivoa.net/std/TAP> .\n",
    "    FILTER (?d IN (dcat:inCatalog, dcat:distribution ) )\n",
    "}\n",
    "\"\"\", initBindings={\"ref\": URIRef(\"https://doi.org/10.3847/1538-4357/ac0af1\")})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60545a84",
   "metadata": {},
   "outputs": [],
   "source": [
    "for doi,dcat,tap,table in result:\n",
    "    print(f\"Dataset {doi} is available \\n\\tin {dcat.fragment} `{table}` \\n\\tthrough the TAP endpoint `{tap}`\\n\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cc282a1e",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,py:percent"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
