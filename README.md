# Demo Notebook for Retreiving Links from Dataset DOIs to other digital objects

This repository contains a Jupyter notebook demonstrating how the various tools and APIs can be used to track the citation of datasets of a repository. The notebook uses a graph database to store the information as triples. 

The example is based on the DOIs with the prefix of Observatoire de Paris-PSL.  

A final section shows how we can add IVOA registry information to the datasets, so that we can find dataset used by a paper and get information on how to access them.

## Requirement

- Python 3.9+
- rdflib
- requests