"""
This file contains the functions and classes used in "PADC DOI Impact monitoring Demo.ipynb"
"""

import requests
from json import JSONDecodeError

from rdflib import URIRef, Literal, BNode, Namespace
from rdflib import Graph
from rdflib.namespace import DCMITYPE, RDF, RDFS, DCTERMS, SDO, FOAF, PROV, OWL, DCAT

# TODO: use idutils

BIBLINK = Namespace("http://www.ivoa.net/rdf/biblink#")
IVOAREL = Namespace("http://www.ivoa.net/rdf/voresource/relationship_type#")
DCITE = Namespace("http://purl.org/spar/datacite/")

DOI_PREFIX = "10.25935"

# mapping from Schema.org to DCMIType:
RESTYPEMAP = {
    "Collection": "Collection",
    "CreativeWork": "InteractiveResource",
    "Dataset": "Dataset",
    "Report": "Text",
    "ScholarlyArticle": "Text",
    "Article": "Text",
    "Service": "Service",
    "SoftwareSourceCode": "Software",
}


# Series of functions to transform short version pids into full fletched URIs.

# Examples:
# doi     | 10.1016/j.pss.2021.105344 | https://doi.org/10.1016/j.pss.2021.105344
# bibcode | 2021P&SS..20905344C       | https://ui.adsabs.harvard.edu/abs/2021P&SS..20905344C
# arxiv   | 2102.07588                | https://arxiv.org/abs/2102.07588


def URIRefDoi(pid):
    return URIRef(f'https://doi.org/{pid.lower()}')


def URIRefBibcode(pid):
    return URIRef(f"https://ui.adsabs.harvard.edu/abs/{pid}")


def URIRefArXiv(pid):
    return URIRef(f"https://arxiv.org/abs/{pid}")


class Report(Graph):
    """The Report class is a rdflib Graph class with a series of extra methods.
    """
    
    def get_obsparis_dois(self):
        """Initialize with all DOIs registered for ObsParis and PADC.
        """
        
        return self.get_dois_from_prefix(doi_prefix=DOI_PREFIX)

    def get_dois_from_prefix(self, doi_prefix):
        """Initialize with all DOIs registered for the provided DOI prefix.
        """    
        
        self.bind("biblink", BIBLINK)
        self.bind("ivoarel", IVOAREL)
        self.bind("dcite", DCITE)

        page_size = 50
        access_url = f"https://api.datacite.org/dois?prefix={doi_prefix}&page%5Bsize%5D={page_size}"
        
        while True:
            # While there is a next page, repeat the query and load DOIs
        
            response = requests.get(access_url)
            data = response.json()
        
            for item in data['data']:
                # for each DOI (as a URI)
                doi = URIRefDoi(item['attributes']['doi'].lower())
                # DataCite is the DOI metadata manager:
                self.add((doi, PROV.wasInformedBy, Literal("DataCite")))
                # ObsParis is the publisher: 
                self.add((doi, DCTERMS.publisher, Literal("ObsParis")))
                # the PID is a DOI
                self.add((doi, BIBLINK.scheme, Literal("doi")))
                # the title:
                self.add((doi, DCTERMS.title, Literal(item["attributes"]["titles"][0]["title"])))
                # the schema.org and DCMI types:
                self.add((doi, RDF.type, SDO[item["attributes"]["types"]["schemaOrg"]]))
                self.add((doi, RDF.type, DCMITYPE[RESTYPEMAP[item["attributes"]["types"]["schemaOrg"]]]))

                # Creators:
                for creator in item["attributes"]["creators"]:
                    if len(creator["nameIdentifiers"]) > 0: 
                        # if there is a NameIdentifier (ORCID)
                        creator_id = creator["nameIdentifiers"][0]["nameIdentifier"]
                        self.add((URIRef(creator_id), RDF.type, FOAF.Person))
                        self.add((URIRef(creator_id), FOAF.name, Literal(creator["name"])))
                        self.add((URIRef(creator_id), BIBLINK.scheme, Literal("orcid")))
                        self.add((doi, DCTERMS.creator, URIRef(creator_id)))
                    else: 
                        tmp = BNode()
                        self.add((tmp, RDF.type, FOAF.Person))
                        self.add((tmp, FOAF.name, Literal(creator["name"])))
                        self.add((doi, DCTERMS.creator, tmp))
                
                for reference in item["attributes"]['relatedIdentifiers']:
                    related_id = reference['relatedIdentifier'].lower()
                    related_id_type = reference['relatedIdentifierType'].lower()
                    if related_id_type == 'doi':
                        related_uri = URIRefDoi(related_id)
                    elif related_id_type == 'bibcode':
                        related_uri = URIRefBibcode(related_id)
                    elif related_id_type == 'arxiv':
                        related_uri = URIRefArXiv(related_id)
                    else:
                        related_uri = URIRef(related_id)
                    #print(doi, related_uri)
                    self.add_with_prov(
                        (doi, DCITE[reference['relationType']], related_uri), 
                        prov={PROV.wasInformedBy: Literal("ObsParis")}
                    )
                        
            if "next" in data["links"].keys():
                access_url = data["links"]["next"]
            else: 
                break
    
    
    def pids_from_publisher(self, publisher):
        """select PIDs from a publisher
        """
        pids = []
        for s, p, o in self:
            if p == DCTERMS.publisher and str(o) == publisher:
                pids.append(s)
        return pids
    
    
    def add_with_prov(self, triple, prov=None):
        """Method to add triple and include provenance metadata about the triple.
        """
        if prov is not None:
            bn = BNode()
            s, p, o = triple
            self.add((bn, RDF.type, RDF.Statement))
            self.add((bn, RDF.subject, s))
            self.add((bn, RDF.predicate, p))
            self.add((bn, RDF.object, o))
            for k, v in prov.items():
                self.add((bn, k, v))
        self.add(triple)
    
    
    def get_biblinks(
        self, 
        access_url="http://voparis-tap-maser.obspm.fr/__system__/biblinks/links/biblinks.json"
    ):
        """Method to retreive metadata from a Biblinks endpoint (early prototype phase), which
        is a proposed method to publish links from papers to datasets or dataservices in the frame
        of the IVOA. This is a very preliminary solution, which may change in the future.
        """
        
        self.bind("ivoarel", IVOAREL)
        
        try: 
            response = requests.get(access_url)
            data = response.json()
            for item in data: 
                data_doi = item['dataset-ref'].lower()
                bib_ref = item['bib-ref']
                relation = item['relationship']
                pid_type = item.get('bib-format', 'bibcode')
        
                if pid_type == 'bibcode':
                    bib_pid = URIRefBibcode(bib_ref)
                elif pid_type == 'doi':
                    bib_pid = URIRefDoi(bib_ref)
        
                subj = bib_pid
                pred = IVOAREL[relation]
                obj = URIRef(data_doi)
        
                self.add_with_prov(
                    (subj, pred, obj), 
                    prov={PROV.wasInformedBy: Literal("biblinks")}
                )
                self.add((subj, BIBLINK.scheme, Literal(pid_type)))

        except JSONDecodeError as e:
            print(doi, e)
            
    
    def get_scholexplorer(self):
        """Harvest Scholexplorer API
        """
        API_URL = "http://api.scholexplorer.openaire.eu/v1/linksFromPid?pid="

        for pid in self.pids_from_publisher("ObsParis"):
            doi = str(pid).replace('https://doi.org/', '')
            access_url = f"{API_URL}{doi.replace('/', '%2F')}"
            try: 
                response = requests.get(access_url)
                data = response.json()
                print(f"{doi}: {len(data)}")
                if len(data) > 0:
                    citation_set = set()
                    for item in data: 
                        for identifiers in item['target']['identifiers']:
                            citation_set.add((identifiers['identifier'], identifiers['schema']))
                    print(f"citing: {', '.join([f'{cite_item[1]}:{cite_item[0]}' for cite_item in citation_set])}")
                    src_pid = URIRefDoi(doi.lower())
                    for identifier, schema in citation_set:
                        if schema == "doi":
                            bib_pid = URIRefDoi(identifier)
                        elif schema == "arXiv":
                            bib_pid = URIRefArXiv(identifier)
                        elif schema == "handle":
                            bib_pid = URIRef(f"http://hdl.handle.net/{identifier}")
                        elif schema == "pmc":
                            bib_pid = URIRef(f"https://www.ncbi.nlm.nih.gov/pmc/articles/{identifier}/")
                        elif schema == "pmid":
                            bib_pid = URIRef(f"https://pubmed.ncbi.nlm.nih.gov/{identifier}/")
                        else:
                            bib_pid = None

                        if bib_pid is not None:

                            # Add triples both ways: 

                            # - data "isReferencedBy" bibref  
                            self.add_with_prov(
                                (src_pid, DCTERMS.isReferencedBy, bib_pid), 
                                prov={PROV.wasInformedBy: Literal("scholexplorer")}
                            )

                            # - bibref "references" data
                            self.add_with_prov(
                                (bib_pid, DCTERMS.references, src_pid), 
                                prov={PROV.wasInformedBy: Literal("scholexplorer")}
                            )

                            self.add((bib_pid, BIBLINK.scheme, Literal(schema)))

            except JSONDecodeError as e:
                print(doi, e)

    
    def get_opencitation(self):
        """Harvest OpenCitations API
        """
        API_URL = "https://opencitations.net/index/api/v1"
        
        for pid in self.pids_from_publisher("ObsParis"):
            doi = str(pid).replace('https://doi.org/', '')
            
            access_url = f"{API_URL}/citations/{doi}"
            try: 
                response = requests.get(access_url)
                data = response.json()
                print(f"{doi}: {len(data)}")
                if len(data) > 0:
                    citation_set = set((item['citing'], 'doi') for item in data)
                    print(f"citing: {', '.join([f'doi:{cite_item[0]}' for cite_item in citation_set])}")

                    src_pid = URIRefDoi(doi)
                    for identifier, schema in citation_set:
                        bib_pid = URIRef(f"https://doi.org/{identifier}".lower())

                        self.add_with_prov(
                            (src_pid, DCTERMS.isReferencedBy, bib_pid),
                            prov={PROV.wasInformedBy: Literal("OpenCitation")} 
                        )
                        self.add_with_prov(
                            (bib_pid, DCTERMS.references, src_pid),
                            prov={PROV.wasInformedBy: Literal("OpenCitation")} 
                        )
                        self.add((bib_pid, BIBLINK.scheme, Literal("doi")))

            except JSONDecodeError as e:
                print(doi, e)
                
    def get_crossref_eventdata(self):
        """Harvest CrossRef EventData API
        """
        API_URL = "https://api.eventdata.crossref.org/v1/events?mailto=baptiste.cecconi@obspm.fr&rows=1000&subj-id="

        for pid in self.pids_from_publisher("ObsParis"):
            doi = str(pid).replace('https://doi.org/', '')
            access_url = f"{API_URL}{doi}"
            try: 
                response = requests.get(access_url)
                data = response.json()
                ndata = len(data['message']['events'])
                print(f"{doi}: {ndata}")
                if ndata > 0:
                    citation_set = set()
                    for event in data['message']['events']:
                        source_pid = event['obj_id']
                        print(source_pid)
                        if source_pid.startswith('https://doi.org/'):
                            item = (source_pid.replace('https://doi.org/', ''), 'doi')
                        else:
                            item = (source_pid, 'pid')
                            break
                        citation_set.add(item)
                    print(f"citing: {', '.join([f'{cite_item[1]}:{cite_item[0]}' for cite_item in citation_set])}")

                    src_pid = URIRefDoi(doi)
                    # dirty fix: 
                    if "202346913" in event['obj_id']:
                        bib_pid = URIRef("https://doi.org/10.1051/0004-6361/202346913")
                    else:
                        bib_pid = URIRef(event['obj_id'].lower())

                    self.add_with_prov(
                        (src_pid, DCTERMS.isReferencedBy, bib_pid),
                        prov={PROV.wasInformedBy: Literal("Crossref_EventData")}
                    )
                    self.add_with_prov(
                        (bib_pid, DCTERMS.references, src_pid),
                        prov={PROV.wasInformedBy: Literal("Crossref_EventData")}
                    )
                    self.add((bib_pid, BIBLINK.scheme, Literal("doi")))

            except JSONDecodeError as e:
                print(doi, e)
